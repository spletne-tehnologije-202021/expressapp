const express = require('express');
const app = express();

const path = require('path');
const uuid = require('uuid');
const avtomobili = require('./Avtomobil');
const mojLogger = require('./mojLogger');
const avtomobiliRouter = require('./routes/api/avtomobili');
const exphbs = require('express-handlebars');

app.use(mojLogger);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

app.get('/', (req, res) => res.render('index', {
    naslov: 'AvtomobiliApp',
    avtomobili: avtomobili
}));

app.get('/nov', (req, res) => res.render('novavto', {
    naslov: 'AvtomobiliApp'
}));


app.use('/web', express.static(path.join(__dirname, 'public')));

app.use('/api/avtomobili', avtomobiliRouter);




// app.get('/', (req, res) => {
//     res.sendFile(path.join(__dirname, 'public', 'index.html'));
// });

// app.post('/', (req, res) => {
//     res.send('Metoda POST');
// });


// app.put('/', (req, res) => {
//     res.send('Metoda PUT');
// });


// app.delete('/', (req, res) => {
//     res.send('Metoda DELETE');
// });



app.listen(4000, () => {
    console.log('Strežnik teče in se odziva na http://localhost:4000');
})