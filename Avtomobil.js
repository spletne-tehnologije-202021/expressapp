const uuid = require('uuid');

const avtomobili = [{
        id: uuid.v4(),
        znamka: 'Renault',
        model: 'Clio',
        letnik: 2005,
        cena: 400
    },
    {
        id: uuid.v4(),
        znamka: 'BMW',
        model: 'X6',
        letnik: 2015,
        cena: 40000
    },
    {
        id: uuid.v4(),
        znamka: 'Mazda',
        model: 'CX30',
        letnik: 2018,
        cena: 20000
    }
]

module.exports = avtomobili;