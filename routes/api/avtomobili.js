const express = require('express');
const avtomobili = require('../../Avtomobil');
const router = express.Router();
const uuid = require('uuid');

router.get('/', (req, res) => {
    res.json(avtomobili);
})

router.get('/:id', (req, res) => {
    const najden = avtomobili.some(avto => avto.id === req.params.id);

    if (!najden) {
        res.status(404).json({ msg: `Avto z id=${req.params.id} ne obstaja` });
    } else {
        res.json(avtomobili.filter(avto => avto.id === req.params.id));
    }
})

router.post('/', (req, res) => {
    const novAvto = {
        id: uuid.v4(),
        znamka: req.body.znamka,
        model: req.body.model,
        letnik: req.body.letnik,
        cena: req.body.cena
    };

    if (!novAvto.znamka || !novAvto.model) {
        res.status(400).json({ msg: 'Oj, pošlji znamko in/ali model' });
    } else {
        avtomobili.push(novAvto);
        // res.location(`${req.protocol}://${req.get('host')}${req.originalUrl}/${novAvto.id}`)
        //     .json(novAvto);
        res.redirect('/');
    }
})

router.put('/:id', (req, res) => {
    if (req.params.id !== req.body.id) {
        res.status(400).json({ msg: 'Idja nista enaka!' });
    } else {
        const obstaja = avtomobili.some(avto => avto.id === req.params.id);
        if (obstaja) {
            const avtoZaUpdate = req.body;
            avtomobili.forEach(avto => {
                if (avto.id == avtoZaUpdate.id) {
                    avto.znamka = avtoZaUpdate.znamka ? avtoZaUpdate.znamka : avto.znamka;
                    avto.model = avtoZaUpdate.model ? avtoZaUpdate.model : avto.model;
                    avto.letnik = avtoZaUpdate.letnik ? avtoZaUpdate.letnik : avto.letnik;
                    avto.cena = avtoZaUpdate.cena ? avtoZaUpdate.cena : avto.cena;
                }
                res.json({ msg: 'Avto je bil posodobljen', avto });
            })
        } else {
            res.status(404).json({ msg: `Avto z id=${req.params.id} ne obstaja` });
        }
    }
})

router.delete('/:id', (req, res) => {
    let zbrisali = false;

    avtomobili.some(avto => {
        if (avto.id === req.params.id) {
            avtomobili.splice(avtomobili.indexOf(avto), 1);
            zbrisali = true;
        }
    })

    if (zbrisali) {
        res.json({ msg: 'Avto je bil zbrisan' });
    } else {
        res.status(404).json({ msg: `Avto z id=${req.params.id} ne obstaja` });
    }
})

module.exports = router;